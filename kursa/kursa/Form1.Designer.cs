﻿namespace kursa
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.buttonOval = new System.Windows.Forms.Button();
            this.buttonText = new System.Windows.Forms.Button();
            this.buttonFontDialog = new System.Windows.Forms.Button();
            this.buttonBackground = new System.Windows.Forms.Button();
            this.buttonValue = new System.Windows.Forms.Button();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.buttonStar = new System.Windows.Forms.Button();
            this.buttonColorDialog = new System.Windows.Forms.Button();
            this.buttonСircle = new System.Windows.Forms.Button();
            this.buttonSquare = new System.Windows.Forms.Button();
            this.buttonLine = new System.Windows.Forms.Button();
            this.buttonDeleteAll = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.trackBarVal = new System.Windows.Forms.TrackBar();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonMain = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVal)).BeginInit();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BackgroundImage = global::kursa.Properties.Resources._10;
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Controls.Add(this.richTextBox);
            this.panel1.Controls.Add(this.buttonOval);
            this.panel1.Controls.Add(this.buttonText);
            this.panel1.Controls.Add(this.buttonFontDialog);
            this.panel1.Controls.Add(this.buttonBackground);
            this.panel1.Controls.Add(this.buttonValue);
            this.panel1.Controls.Add(this.textBoxX);
            this.panel1.Controls.Add(this.textBoxY);
            this.panel1.Controls.Add(this.buttonStar);
            this.panel1.Controls.Add(this.buttonColorDialog);
            this.panel1.Controls.Add(this.buttonСircle);
            this.panel1.Controls.Add(this.buttonSquare);
            this.panel1.Controls.Add(this.buttonLine);
            this.panel1.Controls.Add(this.buttonDeleteAll);
            this.panel1.Controls.Add(this.buttonDelete);
            this.panel1.Controls.Add(this.trackBarVal);
            this.panel1.Controls.Add(this.button24);
            this.panel1.Controls.Add(this.button23);
            this.panel1.Controls.Add(this.button22);
            this.panel1.Controls.Add(this.button20);
            this.panel1.Controls.Add(this.button19);
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.buttonMain);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(954, 94);
            this.panel1.TabIndex = 1;
            // 
            // richTextBox
            // 
            this.richTextBox.BackColor = System.Drawing.Color.Tan;
            this.richTextBox.Location = new System.Drawing.Point(368, 22);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(100, 69);
            this.richTextBox.TabIndex = 36;
            this.richTextBox.Text = "";
            // 
            // buttonOval
            // 
            this.buttonOval.BackColor = System.Drawing.Color.Tan;
            this.buttonOval.BackgroundImage = global::kursa.Properties.Resources.unnamed;
            this.buttonOval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonOval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOval.Location = new System.Drawing.Point(155, 22);
            this.buttonOval.Name = "buttonOval";
            this.buttonOval.Size = new System.Drawing.Size(34, 30);
            this.buttonOval.TabIndex = 35;
            this.buttonOval.UseVisualStyleBackColor = false;
            this.buttonOval.Click += new System.EventHandler(this.buttonOval_Click);
            // 
            // buttonText
            // 
            this.buttonText.BackColor = System.Drawing.Color.Tan;
            this.buttonText.BackgroundImage = global::kursa.Properties.Resources.hotpng_com;
            this.buttonText.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonText.Location = new System.Drawing.Point(508, 62);
            this.buttonText.Name = "buttonText";
            this.buttonText.Size = new System.Drawing.Size(34, 29);
            this.buttonText.TabIndex = 4;
            this.buttonText.UseVisualStyleBackColor = false;
            this.buttonText.Click += new System.EventHandler(this.buttonText_Click);
            // 
            // buttonFontDialog
            // 
            this.buttonFontDialog.BackColor = System.Drawing.Color.Tan;
            this.buttonFontDialog.BackgroundImage = global::kursa.Properties.Resources.kisspng_computer_icons_font_family_font_font_style_5ae193744d7530_0805736115247327883173;
            this.buttonFontDialog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonFontDialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFontDialog.Location = new System.Drawing.Point(508, 22);
            this.buttonFontDialog.Name = "buttonFontDialog";
            this.buttonFontDialog.Size = new System.Drawing.Size(34, 32);
            this.buttonFontDialog.TabIndex = 5;
            this.buttonFontDialog.UseVisualStyleBackColor = false;
            this.buttonFontDialog.Click += new System.EventHandler(this.buttonFontDialog_Click);
            // 
            // buttonBackground
            // 
            this.buttonBackground.BackColor = System.Drawing.Color.Tan;
            this.buttonBackground.BackgroundImage = global::kursa.Properties.Resources.IMGBIN_computer_icons_paint_tool_bucket_png_yGA8KiZD;
            this.buttonBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonBackground.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackground.Location = new System.Drawing.Point(706, 65);
            this.buttonBackground.Name = "buttonBackground";
            this.buttonBackground.Size = new System.Drawing.Size(30, 23);
            this.buttonBackground.TabIndex = 33;
            this.buttonBackground.UseVisualStyleBackColor = false;
            this.buttonBackground.Click += new System.EventHandler(this.buttonBackground_Click);
            // 
            // buttonValue
            // 
            this.buttonValue.BackColor = System.Drawing.Color.Tan;
            this.buttonValue.BackgroundImage = global::kursa.Properties.Resources._81431;
            this.buttonValue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonValue.FlatAppearance.BorderSize = 0;
            this.buttonValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonValue.Location = new System.Drawing.Point(294, 42);
            this.buttonValue.Name = "buttonValue";
            this.buttonValue.Size = new System.Drawing.Size(25, 25);
            this.buttonValue.TabIndex = 32;
            this.buttonValue.UseVisualStyleBackColor = false;
            this.buttonValue.Click += new System.EventHandler(this.buttonValue_Click);
            // 
            // textBoxX
            // 
            this.textBoxX.BackColor = System.Drawing.Color.Tan;
            this.textBoxX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxX.Location = new System.Drawing.Point(254, 56);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(34, 20);
            this.textBoxX.TabIndex = 4;
            this.textBoxX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxY
            // 
            this.textBoxY.BackColor = System.Drawing.Color.Tan;
            this.textBoxY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxY.Location = new System.Drawing.Point(254, 32);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(34, 20);
            this.textBoxY.TabIndex = 5;
            this.textBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonStar
            // 
            this.buttonStar.BackColor = System.Drawing.Color.Tan;
            this.buttonStar.BackgroundImage = global::kursa.Properties.Resources.star__v1;
            this.buttonStar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonStar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStar.Location = new System.Drawing.Point(195, 56);
            this.buttonStar.Name = "buttonStar";
            this.buttonStar.Size = new System.Drawing.Size(34, 30);
            this.buttonStar.TabIndex = 31;
            this.buttonStar.UseVisualStyleBackColor = false;
            this.buttonStar.Click += new System.EventHandler(this.buttonStar_Click);
            // 
            // buttonColorDialog
            // 
            this.buttonColorDialog.BackColor = System.Drawing.Color.Tan;
            this.buttonColorDialog.BackgroundImage = global::kursa.Properties.Resources.roller_brush;
            this.buttonColorDialog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonColorDialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColorDialog.Location = new System.Drawing.Point(706, 23);
            this.buttonColorDialog.Name = "buttonColorDialog";
            this.buttonColorDialog.Size = new System.Drawing.Size(30, 37);
            this.buttonColorDialog.TabIndex = 4;
            this.buttonColorDialog.UseVisualStyleBackColor = false;
            this.buttonColorDialog.Click += new System.EventHandler(this.buttonColorDialog_Click);
            // 
            // buttonСircle
            // 
            this.buttonСircle.BackColor = System.Drawing.Color.Tan;
            this.buttonСircle.BackgroundImage = global::kursa.Properties.Resources.icons8_неотмеченный_круг_50;
            this.buttonСircle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonСircle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonСircle.Location = new System.Drawing.Point(155, 56);
            this.buttonСircle.Name = "buttonСircle";
            this.buttonСircle.Size = new System.Drawing.Size(34, 30);
            this.buttonСircle.TabIndex = 4;
            this.buttonСircle.UseVisualStyleBackColor = false;
            this.buttonСircle.Click += new System.EventHandler(this.buttonСircle_Click);
            // 
            // buttonSquare
            // 
            this.buttonSquare.BackColor = System.Drawing.Color.Tan;
            this.buttonSquare.BackgroundImage = global::kursa.Properties.Resources._354160_preview;
            this.buttonSquare.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSquare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSquare.Location = new System.Drawing.Point(195, 22);
            this.buttonSquare.Name = "buttonSquare";
            this.buttonSquare.Size = new System.Drawing.Size(34, 30);
            this.buttonSquare.TabIndex = 29;
            this.buttonSquare.UseVisualStyleBackColor = false;
            this.buttonSquare.Click += new System.EventHandler(this.buttonSquare_Click);
            // 
            // buttonLine
            // 
            this.buttonLine.BackColor = System.Drawing.Color.Tan;
            this.buttonLine.BackgroundImage = global::kursa.Properties.Resources.IMGBIN_pencil_drawing_tool_computer_icons_png_CL7q5ieV;
            this.buttonLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLine.Location = new System.Drawing.Point(653, 37);
            this.buttonLine.Name = "buttonLine";
            this.buttonLine.Size = new System.Drawing.Size(26, 33);
            this.buttonLine.TabIndex = 4;
            this.buttonLine.UseVisualStyleBackColor = false;
            this.buttonLine.Click += new System.EventHandler(this.buttonLine_Click);
            // 
            // buttonDeleteAll
            // 
            this.buttonDeleteAll.BackColor = System.Drawing.Color.Tan;
            this.buttonDeleteAll.BackgroundImage = global::kursa.Properties.Resources.delete;
            this.buttonDeleteAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonDeleteAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeleteAll.Location = new System.Drawing.Point(12, 56);
            this.buttonDeleteAll.Name = "buttonDeleteAll";
            this.buttonDeleteAll.Size = new System.Drawing.Size(29, 30);
            this.buttonDeleteAll.TabIndex = 4;
            this.buttonDeleteAll.UseVisualStyleBackColor = false;
            this.buttonDeleteAll.Click += new System.EventHandler(this.buttonDeleteAll_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.Tan;
            this.buttonDelete.BackgroundImage = global::kursa.Properties.Resources.clean;
            this.buttonDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonDelete.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Location = new System.Drawing.Point(12, 23);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(29, 30);
            this.buttonDelete.TabIndex = 4;
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // trackBarVal
            // 
            this.trackBarVal.BackColor = System.Drawing.Color.Tan;
            this.trackBarVal.Location = new System.Drawing.Point(587, 23);
            this.trackBarVal.Maximum = 20;
            this.trackBarVal.Minimum = 3;
            this.trackBarVal.Name = "trackBarVal";
            this.trackBarVal.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarVal.Size = new System.Drawing.Size(45, 63);
            this.trackBarVal.TabIndex = 28;
            this.trackBarVal.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarVal.Value = 3;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.Gray;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button24.Location = new System.Drawing.Point(779, 22);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(21, 23);
            this.button24.TabIndex = 26;
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Location = new System.Drawing.Point(779, 42);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(21, 23);
            this.button23.TabIndex = 25;
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.Black;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Location = new System.Drawing.Point(779, 63);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(21, 23);
            this.button22.TabIndex = 24;
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Location = new System.Drawing.Point(806, 22);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(21, 23);
            this.button20.TabIndex = 22;
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.Maroon;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Location = new System.Drawing.Point(806, 42);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(21, 23);
            this.button19.TabIndex = 21;
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Location = new System.Drawing.Point(806, 63);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(21, 23);
            this.button18.TabIndex = 20;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Location = new System.Drawing.Point(833, 22);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(21, 23);
            this.button16.TabIndex = 18;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Location = new System.Drawing.Point(833, 42);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(21, 23);
            this.button15.TabIndex = 17;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Location = new System.Drawing.Point(833, 63);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(21, 23);
            this.button14.TabIndex = 16;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Location = new System.Drawing.Point(860, 22);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(21, 23);
            this.button12.TabIndex = 14;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Olive;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Location = new System.Drawing.Point(860, 42);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(21, 23);
            this.button11.TabIndex = 13;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Location = new System.Drawing.Point(860, 63);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(21, 23);
            this.button10.TabIndex = 12;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Location = new System.Drawing.Point(914, 63);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(21, 23);
            this.button9.TabIndex = 11;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Green;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(914, 42);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(21, 23);
            this.button7.TabIndex = 9;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(914, 22);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(21, 23);
            this.button6.TabIndex = 8;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(887, 22);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(21, 23);
            this.button4.TabIndex = 6;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Teal;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(887, 42);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(21, 23);
            this.button3.TabIndex = 5;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(887, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(21, 23);
            this.button2.TabIndex = 4;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // buttonMain
            // 
            this.buttonMain.BackColor = System.Drawing.Color.Black;
            this.buttonMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMain.Location = new System.Drawing.Point(750, 32);
            this.buttonMain.Name = "buttonMain";
            this.buttonMain.Size = new System.Drawing.Size(23, 43);
            this.buttonMain.TabIndex = 3;
            this.buttonMain.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.buttonMain.UseVisualStyleBackColor = false;
            this.buttonMain.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.menuStrip.BackgroundImage = global::kursa.Properties.Resources._9;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(954, 24);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.openToolStripMenuItem.Text = "Відкрити";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.saveToolStripMenuItem.Text = "Зберегти";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.exitToolStripMenuItem.Text = "Вихід";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "jpg";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox1.Location = new System.Drawing.Point(12, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(932, 458);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.Tan;
            this.trackBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.trackBar1.Location = new System.Drawing.Point(67, 23);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 63);
            this.trackBar1.TabIndex = 37;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.BackgroundImage = global::kursa.Properties.Resources._7;
            this.ClientSize = new System.Drawing.Size(954, 594);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "AlmostPhotoshop";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVal)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonMain;
        private System.Windows.Forms.TrackBar trackBarVal;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonDeleteAll;
        private System.Windows.Forms.Button buttonLine;
        private System.Windows.Forms.Button buttonSquare;
        private System.Windows.Forms.Button buttonСircle;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button buttonColorDialog;
        private System.Windows.Forms.Button buttonStar;
        private System.Windows.Forms.Button buttonValue;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.Button buttonBackground;
        private System.Windows.Forms.Button buttonText;
        private System.Windows.Forms.Button buttonFontDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button buttonOval;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.TrackBar trackBar1;
    }
}

